::: title 基础使用
:::

::: demo

<template>
  <lay-color-picker></lay-color-picker>
</template>

<script>
export default {
  setup() {
    return {
    }
  }
}
</script>

::: title icon-picker 属性
:::

::: table

|            |          |     |
| ---------- | -------- | --- |
| v-model    | 默认值   | --  |
| page       | 开启分页 | --  |
| showSearch | 启用搜索 | --  |

:::
