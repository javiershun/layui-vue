::: title 基本介绍
:::

<img src="http://layui-doc.pearadmin.com/static/images/layui/logo-2.png" />

<br>

::: block

layui - vue（谐音：类 UI) 是 一 套 Vue 3.0 的 桌 面 端 组 件 库 , Layui 的 另 一 种 呈 现 方 式

:::

<lay-timeline>
  <lay-timeline-item title="2021年，layui vue 里程碑版本 0.2.0 发布" simple></lay-timeline-item>
  <lay-timeline-item title="2017年，layui 里程碑版本 2.0 发布" simple></lay-timeline-item>
  <lay-timeline-item title="2016年，layui 首个版本发布" simple></lay-timeline-item>
  <lay-timeline-item title="2015年，layui 孵化" simple></lay-timeline-item>
</lay-timeline>
